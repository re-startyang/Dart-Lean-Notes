## 关于Dart的几点重要说明
- 在Dart中所有变量都是一个对象，所有对象都是一个类的实例。每个数字，方法，甚至是Null都是对象。所有的对吸纳更都是集成自Object这个类。（这个说法其实是很像Java的）
- Dart虽然是个强类型语言，但是它可以推测类型。像number就被推测为int.如果你想明确的说不指定类型，可以使用`dynamic`。（这一点其实已经和高版本的jdk很像了， `dynamic` 就像是Java中的范型）
- Dart是支持范型的。比如 List<`int`> 和List<`dynamic`>
- Dart支持高级方法，比如通过一个方法连接到一个类或是对象（static and instance)，
也可以在方法中创建方法
- Dart支持高级变量，通过一个变量连接到一个类或是对象。（static and instance)，instance 变量有时候也被看作是字段或配置参数。
- 和Java不同的是Dart中不用public,private等权限修饰符，如果想声明为私有，可以使用下划线。
- Dart中声明一个变量或者方法必须以小写字母或者_来表示。
- Dart支持三维运算符
- Dart编译工具会报两种问题： warnings和errors.警告是说你的代码可能有问题，但它不阻止你程序的运行。错误可能是编译时错误也可能时运行时错误。编译时错误根本无法运行，运行时错误会抛异常。

## 变量
- 在Dart中因为所有的变量都是个对象，所以初始化的时候默认值都是null.
- 声明变量的时候，如果使用这种：
```dart
var name = 'Bob';
```
Dart在编译时就会猜测类型为String类型，如果明确指定是String类型，可以这样写：
```dart
String name = 'Bob';
```
如果就是不想指定类型，用这种
```dart
dynamic name = 'Bob'
```
### 静态变量
```dart
final / const
```
在java中修饰一个变量是常量，也是用final。
被修饰为静态的变量只会被赋值一次。
const 是一种编译时状态，被const修饰的变量被当成静态变量。一个被final修饰的高级变量或者类变量只在第一次使用的时候初始化。所以也没法对final修饰的变量做赋值操作。
***
关于const的一点说明：
const是可以修饰值的。被const修饰的值赋值给一个非const修饰的变量，是可以对这个变量进行重新赋值的。
比如：
```dart
var foo =  const[];
foo = [1,2,3];
foo = [2,3,4];
print(foo);

const baz = [1,2];
baz = [1,3,4];
print(baz);
```
上面这段代码，允许对foo重新赋值，但是baz那里就会报错。

## Dart 内置数据类型
- numbers
- strings
- booleans
- lists(可以看做是数组)
- sets
- maps
- runes(在字符串中表示Unicode字符)
- symbols(符号)

在Dart中所有的变量都指向对象，所以可以通过构造函数初始化变量。

### numbers
- Dart中的numbers就俩：
```dart
int | double
```
**int**
Dart 中的int类型数据长度不是固定的64位i，而是和平台有关，在Dart VM中它的取值范围：-2<sup>63</sup> to 2<sup>63</sup> - 1
但是如果把它编译成js,范围成了-2<sup>53</sup> to 2<sup>53</sup> - 1
这个问题在stackoverflow上有人提问过，不是说一套代码四处运行么。。感觉这里也是个坑。
**double**
64位双精度浮点数字类型。

***
ps: int类型的值赋值给double类型的变量的时候会自动类型提升。
```dart
double z = 1;
print(z);
```
输出结果为： 1.0
这种用法在dart2.1前是不支持的。
***********
我是做java的，在java中将字符串转换为数字
```java
Integer.valueof("12");
```
如果在Dart中将字符串转换成数字，会显得更灵活一些，我直接copy官网demo吧：
```dart
var one =  int.parse('1');
print(one ==  1);

var onePointOne =  double.parse('1.1');
print(onePointOne ==  1.1);

String oneAsStr =  1.toString();
print(oneAsStr ==  '1');
```
打印结果都是true

### Strings
Dart中字符串是utf-16编码的字符单元。
Dart中字符串有个特点需要记一下：
```dart
${expression}
```

```dart
String s =  "abcdefg";
print('dart has $s');
```
控制台输出的话就是：dart has abcdefg。
可以看到它可以动态拼接字符串，这个是在运行时调用字符串对象的toString()方法。

### Booleans
```dart
bool flag = true;
bool flag = false;
```
这个没什么说的。

### Lists
一句话，在Dart中数组Array就是List。
```dart
var list = [1,2,2,3];
print(list.length);
print(list[1])
```
操作起来和java中数组更像一些。

### Sets
Set的特点是不允许有重复元素。
```dart
var set = [1,2,3,3];
print(set)
```
结果只有[1,2,3]

```dart
final names =  const  <String>{};
names.add("value");
names.add("value");
names.add("world");
print(names);
```
上边那段代码会运行报错的。因为对names变量进行了finnal声明。

### Maps
和Java中的Map一样，Dart中Map是一个（k,v）结构的对象。
```dart
Map gifts = {
null:  Null
};

print(gifts);
```
在java中的map对k,v的是否可以存null有限制，但是在dart中是没有的。
```dart
var maps = Map();
maps['id'] = '1';
maps['name'] = 'dart';
```
可以看到赋值方式和js中是一样的。

### Runes
这东西挺有意思的。
可以直接将字符编码输出为表情符号。
```dart
Runes input =  new  Runes(
'\u2665 \u{1f605} \u{1f60e} \u{1f47b} \u{1f596} \u{1f44d}');
print(new  String.fromCharCodes(input));
```
♥  😅  😎  👻  🖖  👍

### Symbols
这个怎么说呢，之前在配置redis或是mysql的时候，经常使用到，
用（#）+ 名字 注明要更改的东西。
在Dart官网中，是这样说的，在Dart程序中一个Symbol对象通常代表一个操作或者声明。
你可能平时压根就用不到，但是它对于根据名字声明的API来说很重要。
它是一个编译时状态。
说的有些模糊不清，这一块可能需要后续深入了解才能更明白。








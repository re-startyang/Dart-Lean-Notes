## 操作符

dart 有一套自己定义的操作符：
这里我就不再写了，直接copy一份官网的。
![输入图片说明](https://images.gitee.com/uploads/images/2019/0325/161052_ea99b0c2_1538208.png "15634406.png")
如果有过编程基础，上边展示的操作符应该都不陌生.

### 算术运算符
```dart
加： +
减： -
乘： *
除： /
取余： %
取模： ~/
自增： ++var, var++
自减： --var, var--
```
### 比较运算符
```dart
==: 等于
!=: 不等
>: 大于
<: 小于
>=: 大于等于
<=: 小于等于
```

### 类型判断
```dart
as: 类型转换
is: 判断是否是某种类型
is!: 判断是否不是某种类型
```

### 赋值操作符
```dart
=: 赋值
??: 判断是不是null，如果是null，进行赋值
        var a = 3;
        var b;
        b ??= a;(如果b是null，把a赋值给b)
*=:  a *= 3; ==> a = a * 3;
+=: a += 3; ==> a = a + 3;
...
```
赋值操作符很简单就不一一说了。
### 逻辑操作符
```dart
&&: 逻辑与
||: 逻辑非
!: 非
```

### 位操作符
```dart
&: 与
|: 或
^: 异或
<<: 左移
>>: 右移
```

### 条件表达式
```dart
var a = 1 ? true:false;
int  getNum(int a) => a ??  3; 如果 a == null，返回3，否则返回as
```

### 级联操作符
```dart
..
```
首先说下什么是级联操作符，java中有个词叫链式编程，比如：
```java
StringBuilder() sb = new StringBuilder();
sb.append('a').append('b').toString();
```
级联和链式在外表上看上去很像。
```dart
querySelector('#button')
    ..text =  'Confirm'
    ..classes.add('important')
    ..onClick.listen((e)  => window.alert('Confirmed!'));
```
不需要频繁的创建对象，创建一个button的对象后，后续的对属性的修改和方法的调用操作都是针对这个对象来说。
猜测这个特性，会在编写服务和应用的时候对于连续操作的场景会大量使用。

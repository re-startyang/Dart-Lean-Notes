## Functions

Dart是一门面向对象的语言，所以即便是方法也是一个对象，它的类型是Function.
这就意味着方法可以指向变量，也可以作为方法中的参数供其他方法使用。甚至可以让
一个类作为一个方法，这种类叫Callable classes,即回调类。
```dart
bool  isTrule(String args) {
      return args !=  Null;
}
```
定义一个方法注意返回值和参数。
在Dart中是不允许出现这种方法的：
```dart
bool  isTrule(String args) {
      return args !=  Null;
}
bool  isTrule(int args,String name) {
      return args !=  Null;
}
```
java中可以允许方法的重载但是在Dart是不行的，这点倒像是脚本语言。
```dart
bool  isTrue(int a ) => a !=  null;
print(isTrue(3));
```
如果一个方法中只有一个执行函数可以使用上面那种方式写。有点像js中的回调，也像是
java中的lambda表达式。特别需要强调的是“只有一个执行函数”这句话，用 => 这种执行
的语句不能是if，else这种条件判断，但是可以使用三元运算符。

### 可选参数
一个方法中如果由多个参数，如何规定哪些是必传的，哪些是可选的java是一种强类型语言，如果在java方法中定义一个含有多个参数的方法，我们可以把不需要传的那个参数传一个null进去，但还是得传。Dart中更轻巧一些，只需要一些注解就可以实现，就像定义api接口参数一样方便。
比如像下边这样：
```dart
const todone({String name, @required int id})
```
在Dart中，一个方法参数如果是可选参数，那它可以使命名参数也可以是基于位置的参数。
这句话有点绕，还是举两个栗子：
- 可选命名参数
所谓可选命名参数就是下边这种：
定义方法
```dart
int  getLuckyNum({int year,int month,int day}){
    var luckNum = year + month;
    if(day !=  null){
        luckNum = luckNum +  666;
    }
    return luckNum;
}
```
调用方法：
```dart
var result =  getLuckyNum(year:1,month:2);
```
通俗点说就是把参数名和值以k:v的形式传过去。但是方法参数列表需要用｛｝包裹起来

- 可选位置参数
所谓可选位置参数：
定义方法：
```dart
int getLuckyNum(int year,int month,[int day]){
    var luckNum = year + month;
    if(day != null){
        luckNum = luckNum + 666;
    }
    return luckNum;
}
```
调用方法：
```dart
var result =  getLuckyNum(1,2);
```
特点就是在定义方法时将可选参数放到[]中。
在调用的时候day这个参数是可选的，传和不传会影响上边那个方法的执行结果。
这些可选参数方法是可以赋值默认值的，
```dart
int getLuckyNum(int year,int month,[int day = null){
    var luckNum = year + month;
    if(day != null){
        luckNum = luckNum + 666;
    }
    return luckNum;
}
```
对上边的例子稍作修改，day = null是对这个可选字段的默认值，如果调用方法的时候不传，它默认就是null。
ps: 可选位置参数和可选命名参数不能同时存在。而且，必选参数一定在参数列表前边。

### 主方法
main()
所有程序都有一个启动入口，main()方法返回值类型void,参数是一个List<String>的可选参数。

### 方法参数类（大概应该这么称呼。。。)
就是说把一个方法作为参数类进行传递，如下代码：
```dart
void  doFunction(int  num){
    print(num);
}

var list = [1,2,3];

list.forEach(doFunction);
```
doFunction这个方法作为list.forEach()的参数，进行处理。

### 匿名方法
或者写的简单一点，上边学过一个只有一条执行语句的方法写法：
```dart
var list = [1,2,3];
list.forEach((item) =>  print(item));
```
像上边这种连方法名字都被隐藏起来只有数据流处理的方法，就是匿名方法。

### 词法作用域
```dart
bool top =  true;
main(List<String> args) {
    var insideFlag =  true;
    void  fun1() {
        var fun1Flag =  true;
        void  fun2() {
            print('start');
            print(insideFlag);
            var fun2Flag =  true;
            print(top);
            print(insideFlag);
            print(fun1Flag);
            print(fun2Flag);
        }

    fun2();
}
fun1();
}
```
就像java一样，每个变量都有自己的作用域，其实也就是｛｝内才有效，像那个top可以在这个类的所有地方获取到。

### 测试函数是否相等
之前说过在dart中连方法也是个对象
```dart
void  foo(){}

class  A {
    static  void  bar(){}
    void  baz(){}
}

main(List<String> args) {
    var x;
    x = foo;
    print(x == foo);

    x =  A.bar;
    print(x ==  A.bar);

    var a =  new  A();
    var b =  new  A();
    print(a.baz == b.baz );

    var c =  A();
    print(a ==c);
}
```

### 返回值
每个方法都有一个返回值，如果不写，默认为null
```dart
foo()  {};
assert(foo()  ==  null);
```

